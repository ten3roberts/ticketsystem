# This is a script that defines the build system generation
# Defines files and options and enables generation of both
# Visual Studio projects, Makefiles, and more

cmake_minimum_required(VERSION 3.5)

# Export compile db for clangd
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

project(ticketsystem)

# Specify the files to build in this project
add_executable(${PROJECT_NAME}
  src/database.c
  src/log.c
  src/main.c
  src/menubar.c
  src/notification.c
  src/nuklear_style.c
  src/purchase.c
  src/refund.c
  src/renderer/nuklear.c
  src/renderer/renderer.c
  src/sales.c
  src/tickets.c
  src/utils.c
  src/view/view_helpers.c

  include/controller.h
  include/database.h
  include/log.h
  include/notification.h
  include/nuklear_include.h
  include/nuklear_style.h
  include/purchase.h
  include/renderer/renderer.h
  include/sales.h
  include/tickets.h
  include/utils.h
  include/view/view_helpers.h
  include/view/view_internal.h
)

# Specify include directories to find header files
target_include_directories(${PROJECT_NAME} PRIVATE 
  include 
)

# Include nuklear as a system header to not generate unnused.* warnings
target_include_directories(${PROJECT_NAME} SYSTEM PRIVATE 
  vendor/Nuklear
  vendor/Nuklear/demo/glfw_opengl4
)

# Default to 'Debug' build type if not set
if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
  message("Setting build type to 'Debug' as none was specified.")
  set(CMAKE_BUILD_TYPE Debug CACHE STRING "Choose the type of build." FORCE)
  # Set the possible values of build type for cmake-gui
  set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS "Debug" "Release"
    "MinSizeRel" "RelWithDebInfo")
endif()

# Compiler warnings
if (MSVC)
    target_compile_options(${PROJECT_NAME}
        PRIVATE
        /W4
    )
	set_property(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY VS_STARTUP_PROJECT ${PROJECT_NAME})
else()
    target_compile_options(${PROJECT_NAME} PRIVATE
        -Wall
        -Wextra
        -pedantic
        -Wshadow
        -Wpointer-arith
    )
    # Move compile db to root
    execute_process(COMMAND ln -sf ${CMAKE_BINARY_DIR}/compile_commands.json ${CMAKE_SOURCE_DIR})
endif()

## Add libraries from git submodules ##

# GLFW
add_subdirectory(vendor/glfw)
target_include_directories(${PROJECT_NAME} PRIVATE vendor/glfw/include)

target_link_libraries(${PROJECT_NAME} glfw ${GLFW_LIBRARIES})

# GLEW
# Set OpenGL policy
set(CMAKE_POLICY_DEFAULT_CMP0072 NEW)
add_subdirectory(vendor/glew-2.1.0/build/cmake)
target_include_directories(${PROJECT_NAME} PRIVATE vendor/glew-2.1.0/include)

# Statically link to GLEW
target_link_libraries(${PROJECT_NAME} glew_s)

# SQLite
if(MSVC)
	message("Building sqlite3 from source")
	add_subdirectory(vendor/sqlite3)
	target_link_libraries(${PROJECT_NAME} sqlite3)
else()
	find_package (SQLite3 REQUIRED)
	target_include_directories(${PROJECT_NAME} SYSTEM PRIVATE ${SQLite3_INCLUDE_DIRS})
	target_link_libraries (${PROJECT_NAME} ${SQLite3_LIBRARIES})
endif()

# Copy fonts to resulting binary directory
add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD COMMAND ${CMAKE_COMMAND} -E 
  copy_directory "${PROJECT_SOURCE_DIR}/fonts" "${CMAKE_BINARY_DIR}/fonts")
