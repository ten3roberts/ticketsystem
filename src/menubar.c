#include "menubar.h"
#include "log.h"
#include "style.h"
#include "view/view_helpers.h"

// Draws a menubar with buttons for each item
// `items` is a list of strings and NULL for the last item
// Returns the item clicked or -1
int menubar_draw(struct nk_context *ctx, const char **items, int selected) {
  size_t item_count = 0;
  // Count number of items
  for (item_count = 0; items[item_count] != NULL; item_count++)
    ;

  // Specify a row layout containing all the items
  nk_layout_row_dynamic(ctx, MENUBAR_HEIGHT, item_count);

  // Default, nothing clicked
  int clicked = -1;

  // Draw each item
  for (const char **item = items; *item != NULL; item++) {
    // Button is clicked
    if (ui_tab(ctx, *item, (item - items == selected))) {
      clicked = (int)(item - items);
    }
  }

  return clicked;
}
