#include "refund.h"
#include "controller.h"
#include "log.h"
#include "nuklear_include.h"
#include "style.h"
#include "utils.h"
#include "view/view.h"
#include "view/view_helpers.h"
#include <stdio.h>
#include <stdlib.h>

// Draws all purchase with the customer, price and time of purchase
// Also draws a refund button that will refund the current purchase
static void draw_purchases(struct nk_context *ctx, ControllerRefund *controller,
                           Database *db) {
  // Fetch purchases from database
  // Results are cached, so a call each frame does not query sqlite3
  PurchaseInfo *purchases = database_get_purchases(db);
  size_t purchase_count = database_get_purchase_count(db);

  const float ratios[] = {0.35f, 0.1f, 0.35f, 0.2f};

  // Begin a group that creates a scrollable  subwindow
  if (nk_group_begin(ctx, "Purchases", 0)) {
    // Show column names
    ui_header(ctx, "Available Purchases: ");
    nk_layout_row(ctx, NK_DYNAMIC, TEXT_HEIGHT, 4, ratios);

    nk_button_label(ctx, "Customer");
    nk_button_label(ctx, "Price");
    nk_button_label(ctx, "Time");
    nk_label(ctx, "Refund", NK_TEXT_CENTERED);

    for (size_t i = 0; i < purchase_count; i++) {
      PurchaseInfo *purchase = purchases + i;

      // Customer search query isn't emtpy and isn't a subtring of current
      if (controller->current_search[0] != '\0' &&
          strstr(purchase->name, controller->current_search) == NULL) {
        continue;
      }

      nk_label(ctx, purchase->name, NK_LEFT);

      char buf[256];
      snprintf(buf, sizeof buf, "%ld kr", purchase->price);
      nk_label(ctx, buf, NK_LEFT);

      nk_label(ctx, purchase->time, NK_LEFT);

      // Delete purchase button
      if (nk_button_label(ctx, "Refund")) {
        database_refund(db, purchase->purchase_id);
      }
    }

    // An empty view message
    if (purchase_count == 0) {
      nk_layout_row_dynamic(ctx, TEXT_HEIGHT, 1);
      nk_label(ctx, "No purchases exist.", NK_TEXT_LEFT);
      nk_label(ctx, "Visit `Purchases` tab to add purchases", NK_TEXT_LEFT);
    }
  }
  nk_group_end(ctx);
}

// Draws all refunds from the database
static void draw_refunds(struct nk_context *ctx, ControllerRefund *controller,
                         Database *db) {
  RefundInfo *refunds = database_get_refunds(db);
  size_t refund_count = database_get_refund_count(db);

  const float ratios[] = {0.3f, 0.1f, 0.3f, 0.3f};

  if (nk_group_begin(ctx, "Refunds", 0)) {
    ui_header(ctx, "Refunds: ");
    nk_layout_row(ctx, NK_DYNAMIC, TEXT_HEIGHT, 4, ratios);

    nk_button_label(ctx, "Customer");
    nk_button_label(ctx, "Price");
    nk_button_label(ctx, "Purchase Time");
    nk_button_label(ctx, "Refund Time");

    for (size_t i = 0; i < refund_count; i++) {
      RefundInfo *refund = refunds + i;

      // Customer search query isn't empty and isn't a subtring of current
      // refund name
      if (controller->current_search[0] != '\0' &&
          strstr(refund->name, controller->current_search) == NULL) {
        continue;
      }

      nk_label(ctx, refund->name, NK_LEFT);

      char buf[256];
      snprintf(buf, sizeof buf, "%ld kr", refund->price);
      nk_label(ctx, buf, NK_LEFT);

      nk_label(ctx, refund->purchase_time, NK_LEFT);
      nk_label(ctx, refund->refund_time, NK_LEFT);
    }

    if (refund_count == 0) {
      nk_layout_row_dynamic(ctx, TEXT_HEIGHT, 1);
      nk_label(ctx, "No refunds exist.", NK_TEXT_LEFT);
    }
  }
  nk_group_end(ctx);
}

// Draws the full refund view
void view_refund_draw(struct nk_context *ctx, ControllerRefund *controller,
                      Database *db) {
  ui_header(ctx, "Customer search");
  nk_layout_row_dynamic(ctx, FIELD_HEIGHT, 2);

  nk_edit_string_zero_terminated(ctx, NK_EDIT_FIELD, controller->current_search,
                                 sizeof(controller->current_search) - 1,
                                 nk_filter_default);
  // Scrollable list of all purchases
  float ratios[] = {0.45f, 0.55f};
  nk_layout_row(ctx, NK_DYNAMIC, 400, 2, ratios);

  draw_purchases(ctx, controller, db);
  draw_refunds(ctx, controller, db);

  nk_layout_row_static(ctx, BUTTON_HEIGHT, 100, 1);
}

ControllerRefund controller_refund_create() {
  ControllerHeader header = {.update_func = controller_refund_update};
  return (ControllerRefund){.header = header, .current_search = {0}};
}

void controller_refund_update(void *p, struct nk_context *nk_ctx,
                              Database *db) {
  ControllerRefund *controller = p;

  view_refund_draw(nk_ctx, controller, db);
}
