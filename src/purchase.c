#include "purchase.h"
#include "controller.h"
#include "log.h"
#include "notification.h"
#include "nuklear_include.h"
#include "style.h"
#include "utils.h"
#include "view/view.h"
#include "view/view_helpers.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Appends the receipt to the receipts.txt file
static void save_receipt(ControllerPurchase *controller, Database *db,
                         int64_t purchase_id) {

  // Get the purchase information
  PurchaseInfo purchase = database_get_purchase_info(db, purchase_id);

  // Format the filename
  char fname[256];
  snprintf(fname, sizeof fname, "./receipts.txt");

  // Create and open the receipt file
  // Open in append mode incase duplicate (should'nt really happen)
  FILE *fp = fopen(fname, "a");
  if (fp == NULL) {
    log_err("Failed to open receipt file '%s' for writing", fname);
    return;
  }

  fprintf(fp, "Customer name: %s\n", purchase.name);
  fprintf(fp, "Time of purchase: %s\n", purchase.time);

  // List all tickets in purchase
  fputs("\nTickets:\n", fp);
  for (Ticket *ticket = controller->tickets,
              *end = controller->tickets + controller->ticket_len;
       ticket != end; ticket++) {

    fprintf(fp, "  Ticket: %4ld, %10s, %10s, %10ld\n", ticket->id,
            ticket->event, ticket->class, ticket->price);
  }

  fputs("-----------------------------------------------------\n", fp);

  fprintf(fp, "Total: %ld kr\n", purchase.price);

  fputs("------------------------ END ------------------------\n", fp);

  fclose(fp);
  // Make a buffer large enough for both filename and message
  char buf[sizeof fname + sizeof("Saved receipt to ''")];
  snprintf(buf, sizeof buf, "Saved receipt to '%s'", fname);
  notification_send(NOTIFY_OK, buf);
}

// Clears form data from controller
static void controller_clear(ControllerPurchase *controller) {
  controller->name[0] = '\0';
  controller->ticket_len = 0;
  controller->price_total = 0;
}

// Invoked when the used 'posts' the purchase form
static void controller_purchase_post(ControllerPurchase *controller,
                                     Database *db) {
  if (strlen(controller->name) == 0) {
    log_err("Missing customer name");
    notification_send(NOTIFY_ERROR, "Missing customer name");
    return;
  }

  if (controller->ticket_len == 0) {
    log_err("Purchase must contain atleast 1 ticket");
    notification_send(NOTIFY_ERROR, "Purchase must contain atleast 1 ticket");
    return;
  }

  log_info("Registering tickets");
  log_info(" -> Name: %s", controller->name);

  int64_t purchase_id = database_add_purchase(
      db, controller->name, controller->tickets, controller->ticket_len);

  save_receipt(controller, db, purchase_id);

  // Clear the data from the controller
  controller_clear(controller);
}

// Adds a ticket to the current purchase
static void controller_add_ticket(ControllerPurchase *controller,
                                  Ticket ticket) {
  // Resize to fit
  if (controller->ticket_len + 1 >= controller->ticket_cap) {
    // Set initial capacity
    if (controller->ticket_cap == 0)
      controller->ticket_cap = 4;
    else
      controller->ticket_cap *= 2;

    Ticket *tmp =
        realloc(controller->tickets,
                controller->ticket_cap * sizeof(*controller->tickets));
    if (tmp == NULL) {
      log_err("Failed to realloc memory");
      return;
    }
    controller->tickets = tmp;
  }

  // Update total price
  controller->price_total += ticket.price;
  // Add ticket to the end
  controller->tickets[controller->ticket_len++] = ticket;
}

// Removes a ticket from the current purchase
static void controller_remove_ticket(ControllerPurchase *controller,
                                     Ticket ticket) {
  Ticket prev = {0};
  Ticket save = controller->tickets[controller->ticket_len - 1];
  int found = 0;
  for (size_t i = controller->ticket_len; i != 0 && !found;) {
    i--;

    // Found
    if (controller->tickets[i].id == ticket.id) {
      // Update total price
      controller->price_total -= ticket.price;

      controller->ticket_len--;
      found = 1;
    }

    // Swap the last one with the current one
    prev = save;
    save = controller->tickets[i];
    controller->tickets[i] = prev;
  }
}

static void draw_all_tickets(struct nk_context *ctx,
                             ControllerPurchase *controller, Database *db) {
  Ticket *tickets = database_get_tickets(db);
  size_t ticket_count = database_get_ticket_count(db);

  const float ratios[] = {0.3f, 0.3f, 0.3f, 0.1f};

  if (nk_group_begin(ctx, "Tickets", 0)) {
    ui_header(ctx, "Available Tickets: ");
    nk_layout_row(ctx, NK_DYNAMIC, TEXT_HEIGHT, 4, ratios);

    // Show column name buttons and sort list when clicked
    if (nk_button_label(
            ctx, (controller->current_sort == 0 ? "[EVENT]" : "event"))) {
      controller->current_sort = 0;
      qsort(tickets, ticket_count, sizeof(*tickets), sort_event);
    }
    if (nk_button_label(
            ctx, (controller->current_sort == 1 ? "[CLASS]" : "class"))) {
      controller->current_sort = 1;
      qsort(tickets, ticket_count, sizeof(*tickets), sort_class);
    }
    if (nk_button_label(
            ctx, (controller->current_sort == 2 ? "[PRICE]" : "price"))) {
      controller->current_sort = 2;
      qsort(tickets, ticket_count, sizeof(*tickets), sort_price);
    }
    nk_label(ctx, "Add", NK_TEXT_CENTERED);

    for (size_t i = 0; i < ticket_count; i++) {
      Ticket *ticket = tickets + i;
      nk_label(ctx, ticket->event, NK_LEFT);
      nk_label(ctx, ticket->class, NK_LEFT);
      char buf[256];

      snprintf(buf, sizeof buf, "%ld kr", ticket->price);
      nk_label(ctx, buf, NK_LEFT);

      // Add ticket button
      if (nk_button_label(ctx, "->")) {
        controller_add_ticket(controller, *ticket);
      }
    }
    if (ticket_count == 0) {
      nk_layout_row_dynamic(ctx, TEXT_HEIGHT, 1);
      nk_label(ctx, "No event tickets exist.", NK_TEXT_LEFT);
      nk_label(ctx, "Visit `Tickets` tab to add event tickets", NK_TEXT_LEFT);
    }
  }
  nk_group_end(ctx);
}

static void draw_added_tickets(struct nk_context *ctx,
                               ControllerPurchase *controller) {
  Ticket *tickets = controller->tickets;
  size_t ticket_count = controller->ticket_len;

  const float ratios[] = {0.3f, 0.3f, 0.3f, 0.1f};

  if (nk_group_begin(ctx, "Tickets", 0)) {
    ui_header(ctx, "Purchase Tickets: ");
    nk_layout_row(ctx, NK_DYNAMIC, TEXT_HEIGHT, 4, ratios);

    // Show column name buttons and sort list when clicked
    if (nk_button_label(
            ctx, (controller->current_sort == 0 ? "[EVENT]" : "event"))) {
      controller->current_sort = 0;
      qsort(tickets, ticket_count, sizeof(*tickets), sort_event);
    }
    if (nk_button_label(
            ctx, (controller->current_sort == 1 ? "[CLASS]" : "class"))) {
      controller->current_sort = 1;
      qsort(tickets, ticket_count, sizeof(*tickets), sort_class);
    }
    if (nk_button_label(
            ctx, (controller->current_sort == 2 ? "[PRICE]" : "price"))) {
      controller->current_sort = 2;
      qsort(tickets, ticket_count, sizeof(*tickets), sort_price);
    }
    nk_label(ctx, "Del", NK_TEXT_CENTERED);

    for (size_t i = 0; i < ticket_count; i++) {
      Ticket *ticket = tickets + i;
      nk_label(ctx, ticket->event, NK_LEFT);
      nk_label(ctx, ticket->class, NK_LEFT);
      char buf[256];

      snprintf(buf, sizeof buf, "%ld kr", ticket->price);
      nk_label(ctx, buf, NK_LEFT);

      // Delete ticket button
      if (nk_button_label(ctx, "<-")) {
        controller_remove_ticket(controller, *ticket);
        break;
      }
    }

    // Draw total price
    char buf[256];
    snprintf(buf, sizeof buf, "Total: %ld kr", controller->price_total);
    nk_label(ctx, buf, NK_TEXT_ALIGN_LEFT);
  }
  nk_group_end(ctx);
}

void view_purchase_draw(struct nk_context *ctx, ControllerPurchase *controller,
                        Database *db) {
  ui_header(ctx, "Customer name");
  nk_layout_row_dynamic(ctx, FIELD_HEIGHT, 2);

  nk_edit_string_zero_terminated(ctx, NK_EDIT_FIELD, controller->name,
                                 sizeof(controller->name) - 1,
                                 nk_filter_default);
  ui_header(ctx, "Purchase: ");

  // Scrollable list of all tickets
  nk_layout_row_dynamic(ctx, 300, 2);

  draw_all_tickets(ctx, controller, db);
  draw_added_tickets(ctx, controller);

  nk_layout_row_static(ctx, BUTTON_HEIGHT, 100, 1);

  if (nk_button_label(ctx, "Finish")) {
    controller_purchase_post(controller, db);
    return;
  }
}

ControllerPurchase controller_purchase_create() {
  ControllerHeader header = {.update_func = controller_purchase_update};
  return (ControllerPurchase){.header = header, .name = {0}};
}

void controller_purchase_destroy(ControllerPurchase *controller) {
  if (controller->tickets)
    free(controller->tickets);
}

void controller_purchase_update(void *p, struct nk_context *nk_ctx,
                                Database *db) {
  ControllerPurchase *controller = p;

  view_purchase_draw(nk_ctx, controller, db);
}
