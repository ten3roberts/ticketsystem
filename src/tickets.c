#include "tickets.h"
#include "controller.h"
#include "log.h"
#include "nuklear_include.h"
#include "style.h"
#include "utils.h"
#include "view/view.h"
#include "view/view_helpers.h"
#include <stdio.h>
#include <stdlib.h>
// This view defines the view of creating tickets for an event

// Adds a set of predefined tickets for an event
static void add_example_tickets(Database* db) {
  database_add_ticket(db, (Ticket) { .event = "Mellodifestivalen", .class = "Adult", .price = 100 });
  database_add_ticket(db, (Ticket) { .event = "Mellodifestivalen", .class = "Retiree", .price = 75 });
  database_add_ticket(db, (Ticket) { .event = "Mellodifestivalen", .class = "Child", .price = 25 });
}


// Invoked when the user 'posts' a new ticket type creation form
// Uses the database interface to add the ticket to the ticket table
static void controller_tickets_new_ticket(ControllerTickets *controller,
                                          Database *db) {

  // event or class are empty
  if (strlen(controller->input_event) == 0 ||
      strlen(controller->input_class) == 0) {
    log_err("Missing required field name");
    return;
  }

  // Fill ticket struct from data held in `controller`
  Ticket ticket;
  snprintf(ticket.event, sizeof ticket.event, "%s", controller->input_event);
  snprintf(ticket.class, sizeof ticket.class, "%s", controller->input_class);

  if (sscanf(controller->input_price, "%ld", &ticket.price) != 1) {
    log_err("Invalid price entered for ticket");
    return;
  }

  // Add ticket to database
  database_add_ticket(db, ticket);

  // Reset input fields
  controller->input_class[0] = '\0';
  controller->input_event[0] = '\0';
  controller->input_price[0] = '\0';
}

// Draws the tickets view UI
void view_tickets_draw(struct nk_context *ctx, ControllerTickets *controller,
                       Database *db) {
  (void)controller;
  ui_header(ctx, "Add new ticket");
  nk_layout_row_dynamic(ctx, FIELD_HEIGHT, 3);

  nk_label(ctx, "Event Name", NK_RIGHT);
  nk_label(ctx, "Price/Age Class", NK_RIGHT);
  nk_label(ctx, "Price", NK_RIGHT);

  nk_layout_row_dynamic(ctx, FIELD_HEIGHT, 3);

  nk_edit_string_zero_terminated(ctx, NK_EDIT_FIELD, controller->input_event,
                                 sizeof(controller->input_event) - 1,
                                 nk_filter_default);

  nk_edit_string_zero_terminated(ctx, NK_EDIT_FIELD, controller->input_class,
                                 sizeof(controller->input_class) - 1,
                                 nk_filter_default);

  nk_edit_string_zero_terminated(ctx, NK_EDIT_FIELD, controller->input_price,
                                 sizeof(controller->input_price) - 1,
                                 nk_filter_decimal);

  nk_layout_row_static(ctx, BUTTON_HEIGHT, 100, 1);
  if (nk_button_label(ctx, "Add Ticket")) {
    controller_tickets_new_ticket(controller, db);
  }

  nk_layout_row_static(ctx, BUTTON_HEIGHT, 200, 1);
  if (nk_button_label(ctx, "Add Example Tickets")) {
    add_example_tickets(db);
  }

  // List all tickets
  nk_layout_row_dynamic(ctx, HEADER_HEIGHT, 1);
  ui_header(ctx, "Tickets");

  Ticket *tickets = database_get_tickets(db);
  size_t ticket_count = database_get_ticket_count(db);

  const float ratios[] = {0.3f, 0.3f, 0.3f, 0.1f};
  nk_layout_row(ctx, NK_DYNAMIC, BUTTON_HEIGHT, 4, ratios);

  // Show column name buttons and sort list when clicked
  if (nk_button_label(ctx,
                      (controller->current_sort == 0 ? "[EVENT]" : "event"))) {
    controller->current_sort = 0;
    qsort(tickets, ticket_count, sizeof(*tickets), sort_event);
  }
  if (nk_button_label(ctx,
                      (controller->current_sort == 1 ? "[CLASS]" : "class"))) {
    controller->current_sort = 1;
    qsort(tickets, ticket_count, sizeof(*tickets), sort_class);
  }
  if (nk_button_label(ctx,
                      (controller->current_sort == 2 ? "[PRICE]" : "price"))) {
    controller->current_sort = 2;
    qsort(tickets, ticket_count, sizeof(*tickets), sort_price);
  }
  nk_label(ctx, "Delete", NK_TEXT_CENTERED);

  // Scrollable list of all tickets
  nk_layout_row_dynamic(ctx, 300, 1);
  if (nk_group_begin(ctx, "Tickets", 0)) {
    nk_layout_row(ctx, NK_DYNAMIC, TEXT_HEIGHT, 4, ratios);

    for (size_t i = 0; i < ticket_count; i++) {
      Ticket *ticket = tickets + i;
      nk_label(ctx, ticket->event, NK_TEXT_LEFT);
      nk_label(ctx, ticket->class, NK_TEXT_LEFT);
      char buf[256];

      snprintf(buf, sizeof buf, "%ld kr", ticket->price);
      nk_label(ctx, buf, NK_TEXT_LEFT);

      // Delete ticket button
      if (nk_button_label(ctx, "-")) {
        database_delete_ticket(db, ticket->id);
      }
    }
  }
  nk_group_end(ctx);
}

ControllerTickets controller_tickets_create() {
  ControllerHeader header = {.update_func = controller_tickets_update};
  return (ControllerTickets){.header = header,
                             .input_event = "",
                             .input_class = "",
                             .input_price = ""};
}

void controller_tickets_update(void *p, struct nk_context *nk_ctx,
                               Database *db) {
  ControllerTickets *controller = p;

  view_tickets_draw(nk_ctx, controller, db);
}
