#include "controller.h"
#include "database.h"
#include "log.h"
#include "menubar.h"
#include "notification.h"
#include "purchase.h"
#include "refund.h"
#include "renderer/renderer.h"
#include "sales.h"
#include "tickets.h"
#include <limits.h>
#include <stdio.h>

#ifdef _WIN32
#include <direct.h>
#else
#include <unistd.h>
#endif

// Changes the working directory to the binary location
static void normalize_workdir(const char *prog_path) {
  // Large enough for all paths
  char path[4096];

  // Find last slash
  const char *path_end = strrchr(prog_path, '/');
  if (path_end == NULL)
    path_end = strrchr(prog_path, '\\');

  size_t path_len = 0;
  if (path_end)
    path_len = path_end - prog_path;
  else
    path_len = strlen(prog_path);

  strncpy(path, prog_path, path_len);
  path[path_len] = '\0';

  log_info("Path: '%s'", path);

#ifdef _WIN32
  (void)_chdir(path);
  (void)_chdir("..");
#else
  chdir(path);
  chdir("..");
#endif
}

// Main entry point
int main(int argc, char **argv) {
  (void)argc;

  normalize_workdir(argv[0]);

  Database *db = database_create("ticketsystem.db");
  database_create_tables(db);

  Renderer *renderer = renderer_create(1000, 600, "Ticketsystem");

  // All the controllers for each of the menus
  ControllerPurchase controller_purchase = controller_purchase_create();
  ControllerSales controller_sales = controller_sales_create();
  ControllerRefund controller_refund = controller_refund_create();
  ControllerTickets controller_tickets = controller_tickets_create();

  // Mapping between controller names and index into controllers
  const char *menubar_item_names[] = {"Purchase", "Sales", "Purchases/Refund",
                                      "Tickets", NULL};
  // The currently active view index
  // Start in the purchase view
  int menubar_active = 0;

  // A virtual list of all controllers
  // All controllers have a header with an update function pointer which simulates dynamic dispatch
  ControllerHeader *menubar_item_controllers[4];

  menubar_item_controllers[0] = (ControllerHeader *)&controller_purchase;
  menubar_item_controllers[1] = (ControllerHeader *)&controller_sales;
  menubar_item_controllers[2] = (ControllerHeader *)&controller_refund;
  menubar_item_controllers[3] = (ControllerHeader *)&controller_tickets;

  // The active/focused controller
  ControllerHeader *active_controller =
      (ControllerHeader *)&controller_purchase;

  // Enter the main event loop
  while (renderer_begin(renderer)) {
    // Render the menubar giving it the predefined list of menu item names
    int clicked = menubar_draw(renderer_nk_ctx(renderer), menubar_item_names,
                               menubar_active);

    // Change the active menu/controller if user clicked the menu
    if (clicked != -1) {
      active_controller = menubar_item_controllers[clicked];
      menubar_active = clicked;
    }

    // Draw notifications
    notification_draw(renderer_nk_ctx(renderer));

    // Update the active controller drawing the active view
    active_controller->update_func(active_controller, renderer_nk_ctx(renderer),
                                   db);
    // End rendering and swap the framebuffers
    renderer_end(renderer);
  }

  // Free up resources (only for controllers that require it)
  controller_purchase_destroy(&controller_purchase);

  renderer_destroy(renderer);
  database_destroy(db);

  // Pause windows console
#ifdef _WIN32
  printf("Press enter to exit...\n");
  (void)fgetc(stdin);
#endif
  return 0;
}
