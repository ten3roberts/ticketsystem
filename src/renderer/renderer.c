#include "renderer/renderer.h"
#include "log.h"
#include "view/view.h"

#include "nuklear_include.h"
// Nuklear default styles
#include "nuklear_style.h"
#include "style.h"

#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CLEAR_COLOR_R 0.0f
#define CLEAR_COLOR_G 0.15f
#define CLEAR_COLOR_B 0.2f
#define CLEAR_COLOR_A 1.0f

// Implementation of Renderer
struct Renderer {
  GLFWwindow *window;
  struct nk_context *nk_ctx;
  int width;
  int height;
  struct nk_font *font_normal;
  struct nk_font *font_bold;
};

static void glfw_error_callback(int error, const char *description) {
  log_err("GLFW %d: %s", error, description);
}

// Incremens for each create
// Decrements for each destroy
static int renderer_active_count = 0;

// Create a glfw window
// Does not initialize window
GLFWwindow *renderer_window_create(int width, int height, const char *title) {

  glfwSetErrorCallback(glfw_error_callback);

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#ifdef __APPLE__
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

  GLFWwindow *window = glfwCreateWindow(width, height, title, NULL, NULL);

  if (!window) {
    log_err("Failed to create glfw window");
    glfwTerminate();
    return NULL;
  }

  glfwMakeContextCurrent(window);

  return window;
}

void renderer_window_destroy(GLFWwindow *window) { glfwDestroyWindow(window); }

// Initializes nuklear for current renderer
int renderer_nuklear_init(Renderer *renderer) {
  // Initialize nuklear context
  struct nk_context *nk_ctx =
      nk_glfw3_init(renderer->window, NK_GLFW3_INSTALL_CALLBACKS,
                    MAX_VERTEX_BUFFER, MAX_ELEMENT_BUFFER);

  // Load fonts
  {
    struct nk_font_atlas *atlas;
    nk_glfw3_font_stash_begin(&atlas);
    struct nk_font_config font_config = nk_font_config(16);
    font_config.oversample_h = 4;
    font_config.oversample_v = 4;
    renderer->font_bold = nk_font_atlas_add_from_file(
        atlas, "./fonts/Roboto-Bold.ttf", 18, &font_config);
    renderer->font_normal = nk_font_atlas_add_from_file(
        atlas, "./fonts/DroidSans.ttf", 18, &font_config);
    if (renderer->font_bold == NULL || renderer->font_normal == NULL) {
      log_err("Failed to load font files located at {BINARY}/fonts");
      nk_free(nk_ctx);
      return EXIT_FAILURE;
    }

    nk_glfw3_font_stash_end();
    nuklear_set_style(nk_ctx, THEME_RED);
    nk_style_set_font(nk_ctx, &renderer->font_bold->handle);
    /*nk_style_load_all_cursors(ctx, atlas->cursors);*/
    /*nk_style_set_font(ctx, &droid->handle);*/
  }

  renderer->nk_ctx = nk_ctx;

  return EXIT_SUCCESS;
}

Renderer *renderer_create(int width, int height, const char *title) {
  Renderer *renderer;
  renderer = malloc(sizeof *renderer);

  // First renderer, init glfw and glew
  if (renderer_active_count == 0 && !glfwInit()) {
    log_err("Failed to initialize glfw");
    return NULL;
  }

  renderer_active_count++;

  GLFWwindow *window = renderer_window_create(width, height, title);
  if (window == NULL)
    goto failure;

  renderer->window = window;
  glfwGetWindowSize(renderer->window, &width, &height);

  // Initialize opengl
  if (glewInit() != GLEW_OK) {
    log_err("Failed to initialize GLEW");
    goto failure;
  }

  log_info("Using GLEW %s", glewGetString(GLEW_VERSION));

  if (renderer_nuklear_init(renderer) != EXIT_SUCCESS) {
    log_err("Failed to create nuklear context");
    goto failure;
  }

  return renderer;

  // Cleanup resources in case of failure
failure:
  if (window)
    renderer_window_destroy(window);
  if (renderer)
    free(renderer);

  return NULL;
}

bool renderer_begin(Renderer *renderer) {
  glfwPollEvents();

  // Drawing
  glClearColor(CLEAR_COLOR_R, CLEAR_COLOR_G, CLEAR_COLOR_B, CLEAR_COLOR_A);
  glClear(GL_COLOR_BUFFER_BIT);

  nk_glfw3_new_frame();

  // Update window size
  glfwGetWindowSize(renderer->window, &renderer->width, &renderer->height);
  glViewport(0, 0, renderer->width, renderer->height);

  /* GUI */
  nk_begin(renderer->nk_ctx, "Ticketsystem",
           nk_rect(0, 0, renderer->width, renderer->height), 0);

  return !glfwWindowShouldClose(renderer->window);
}

void renderer_end(Renderer *renderer) {
  nk_end(renderer->nk_ctx);

  nk_glfw3_render(NK_ANTI_ALIASING_ON);

  glfwSwapBuffers(renderer->window);
}

void renderer_destroy(Renderer *renderer) {
  renderer_window_destroy(renderer->window);

  renderer_active_count--;
  // Destroys the nuklear context and glfw device
  nk_glfw3_shutdown();

  if (renderer_active_count == 0) {
    glfwTerminate();
  }

  free(renderer);
}

struct nk_context *renderer_nk_ctx(Renderer *renderer) {
  return renderer->nk_ctx;
}
