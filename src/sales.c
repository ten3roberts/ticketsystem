#include "sales.h"
#include "controller.h"
#include "log.h"
#include "nuklear_include.h"
#include "style.h"
#include "utils.h"
#include "view/view.h"
#include "view/view_helpers.h"
#include <stdio.h>

// Draws a summary in a subwindow of all ticket types sold
static void draw_all_tickets(struct nk_context *ctx,
                             Database *db) {
  // Fetch purchases from database
  // Results are cached, so a call each frame does not query sqlite3
  TicketSale *sales = database_get_ticket_sales(db);
  size_t sale_count = database_get_ticket_sale_count(db);
  // Accumulate a total sum of all sold tickets
  // This way does not require to do a SELECT SUM...
  size_t total_revenue = 0;
  size_t total_tickets = 0;

  if (nk_group_begin(ctx, "Tickets", 0)) {
    nk_layout_row_dynamic(ctx, TEXT_HEIGHT, 5);

    nk_button_label(ctx, "event");
    nk_button_label(ctx, "class");
    nk_button_label(ctx, "unit price");
    nk_button_label(ctx, "count");
    nk_button_label(ctx, "total price");

    for (size_t i = 0; i < sale_count; i++) {
      TicketSale *sale = sales + i;
      nk_label(ctx, sale->ticket.event, NK_TEXT_LEFT);
      nk_label(ctx, sale->ticket.class, NK_TEXT_LEFT);
      char buf[256];

      snprintf(buf, sizeof buf, "%ld kr", sale->ticket.price);
      nk_label(ctx, buf, NK_TEXT_LEFT);
      snprintf(buf, sizeof buf, "%ld", sale->count);
      nk_label(ctx, buf, NK_TEXT_LEFT);

      snprintf(buf, sizeof buf, "%ld kr", sale->ticket.price * sale->count);
      nk_label(ctx, buf, NK_TEXT_LEFT);

      total_revenue += sale->ticket.price * sale->count;
      total_tickets += sale->count;
    }

    if (sale_count == 0) {
      nk_layout_row_dynamic(ctx, TEXT_HEIGHT, 1);
      nk_label(ctx, "No tickets sold", NK_TEXT_LEFT);
    }

    // Total stats
    nk_layout_row_dynamic(ctx, TEXT_HEIGHT, 1);
    // Separator
    nk_label(ctx, "", NK_TEXT_LEFT);
    char buf[256];
    snprintf(buf, sizeof buf, "Total tickets sold: %ld", total_tickets);
    nk_label(ctx, buf, NK_TEXT_LEFT);

    snprintf(buf, sizeof buf, "Total revenue: %ld kr", total_revenue);
    nk_label(ctx, buf, NK_TEXT_LEFT);
  }
  nk_group_end(ctx);
}

// Draws the sales view
void view_sales_draw(struct nk_context *ctx, ControllerSales *controller,
                     Database *db) {
  (void)controller;
  ui_header(ctx, "Sales");
  nk_layout_row_dynamic(ctx, TEXT_HEIGHT, 1);

  nk_label(ctx, "Total sales: ", NK_LEFT);
  // Scrollable list of all sales
  nk_layout_row_dynamic(ctx, 400, 1);
  draw_all_tickets(ctx, db);
}

ControllerSales controller_sales_create() {
  ControllerHeader header = {.update_func = controller_sales_update};
  return (ControllerSales){.header = header};
}

void controller_sales_update(void *p, struct nk_context *nk_ctx, Database *db) {
  ControllerSales *controller = p;

  view_sales_draw(nk_ctx, controller, db);
}
