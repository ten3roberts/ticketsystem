#include "database.h"
#include "log.h"
#include "utils.h"
#include <assert.h>
#include <sqlite3.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SQL_MAXLEN 1024

// Macro to return error code if non-zero to callee
#define TRY(f)                                                                 \
  {                                                                            \
    int __try_result = f;                                                      \
    if (__try_result != EXIT_SUCCESS)                                          \
      return __try_result;                                                     \
  }

// Definition of opaque database pointer
struct Database {
  sqlite3 *sqlite;
  size_t ticket_count;
  Ticket *tickets;

  TicketSale *ticket_sales;
  size_t ticket_sale_count;

  PurchaseInfo *purchases;
  size_t purchase_count;

  RefundInfo *refunds;
  size_t refund_count;
  bool out_of_date;
};

// Query callback that will print the sql results using log_info
// Primarily used for debugging
static int query_callback(void *userdata, int num_columns, char **column_values,
                          char **column_names) {
  (void)userdata;
  int i;
  for (i = 0; i < num_columns; i++) {
    log_info("SQLITE: %s = %s", column_names[i],
             column_values[i] ? column_values[i] : "NULL");
  }
  log_info("----------------");
  return 0;
}

// Called whenever something changes in the database
// Triggers a refresh of the internally cached results
static void update_callback(void *userdata, int opcode, const char *db_name,
                            const char *table_name, sqlite3_int64 rowid) {
  // Ignore some arguments
  (void)opcode;
  (void)db_name;
  (void)table_name;
  (void)rowid;

  Database *db = userdata;
  // Next request for tickets or similar will query the database and refresh the
  // cache
  db->out_of_date = true;
}

Database *database_create(const char *filename) {
  sqlite3 *sqlite;
  int result;

  result = sqlite3_open(filename, &sqlite);
  if (result != SQLITE_OK) {
    log_err("Failed to open database '%s': %s", filename,
            sqlite3_errmsg(sqlite));
    return NULL;
  }

  log_info("Opened database '%s'", filename);

  Database *db;
  db = malloc(sizeof *db);
  db->sqlite = sqlite;

  db->out_of_date = true;

  db->ticket_count = 0;
  db->tickets = NULL;

  db->ticket_sales = NULL;
  db->ticket_sale_count = 0;

  db->purchases = NULL;
  db->purchase_count = 0;

  db->refunds = NULL;
  db->refund_count = 0;

  sqlite3_update_hook(sqlite, update_callback, db);

  return db;
}

int database_create_tables(Database *db) {
  if (db == NULL)
    return EXIT_FAILURE;

  // Contains information about each customer/purchase
  // A customer/purchase can contain multiple tickets, and is a one-to-many
  // relationship using `purchase_tickets`
  // Does not contain refunded purchases
  const char *purchase_table = "CREATE TABLE IF NOT EXISTS PURCHASE("
                               "PURCHASE_ID INTEGER PRIMARY KEY AUTOINCREMENT,"
                               "CUSTOMER TEXT NOT NULL COLLATE NOCASE,"
                               "PRICE INTEGER,"
                               "TIME DATETIME DEFAULT CURRENT_TIMESTAMP );";

  // Contains information regarding refunds
  // Does not contain the tickets of the refunds, but the total price and
  // purchase date, aswell as refund date
  const char *refund_table =
      "CREATE TABLE IF NOT EXISTS REFUND("
      "REFUND_ID INTEGER PRIMARY KEY AUTOINCREMENT,"
      "CUSTOMER TEXT NOT NULL COLLATE NOCASE,"
      "PRICE INTEGER,"
      "PURCHASE_TIME DATETIME,"
      "REFUND_TIME DATETIME DEFAULT CURRENT_TIMESTAMP );";

  // Contains a one-to-many relationship between a customer's purchase and the
  // ticket types
  const char *purchase_tickets_table =
      "CREATE TABLE IF NOT EXISTS PURCHASE_TICKETS("
      "PURCHASE_ID INTEGER,"
      "TICKET_ID INTEGER,"
      "FOREIGN KEY (PURCHASE_ID) REFERENCES PURCHASE(PURCHASE_ID),"
      "FOREIGN KEY (TICKET_ID) REFERENCES TICKET(TICKET_ID)"
      ");";

  // Contains the different types of tickets
  const char *tickets_table =
      "CREATE TABLE IF NOT EXISTS TICKET("
      "TICKET_ID INTEGER PRIMARY KEY AUTOINCREMENT,"
      "EVENT TEXT NOT NULL COLLATE NOCASE,"
      "CLASS TEXT NOT NULL COLLATE NOCASE,"
      "PRICE INTEGER NOT NULL,"
      "UNIQUE(EVENT, CLASS)" // Only one ticket with the same event and class
      ");";

  // Create the tables
  TRY(database_query(db, purchase_table));
  TRY(database_query(db, refund_table));
  TRY(database_query(db, purchase_tickets_table));
  TRY(database_query(db, tickets_table));

  return EXIT_SUCCESS;
}

// Queries and returns a statement object
sqlite3_stmt *database_query_result(Database *db, const char *sql) {
  sqlite3_stmt *result;
  if (sqlite3_prepare_v2(db->sqlite, sql, -1, &result, NULL) != SQLITE_OK) {
    log_err("SQL Error: %s", sqlite3_errmsg(db->sqlite));
    return NULL;
  }
  return result;
}

int database_query(Database *db, const char *sql) {
  char *errmsg = NULL;
  int result = sqlite3_exec(db->sqlite, sql, query_callback, NULL, &errmsg);
  if (result != SQLITE_OK) {
    log_err("SQL Error: %s", errmsg);
    sqlite3_free(errmsg);
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

// Returns the number of rows in table
static size_t database_get_rowcount(Database *db, const char *table) {
  sqlite3_stmt *res;
  char sql[SQL_MAXLEN];
  sqlite3_snprintf(sizeof sql, sql, "SELECT COUNT(*) FROM %s", table);
  // Get number of tickets
  res = database_query_result(db, sql);

  sqlite3_step(res);
  size_t count = sqlite3_column_int64(res, 0);

  sqlite3_finalize(res);
  return count;
}

// Returns the integer result of query
static size_t database_get_int(Database *db, const char *sql) {
  sqlite3_stmt *res = database_query_result(db, sql);
  sqlite3_step(res);
  size_t count = sqlite3_column_int64(res, 0);
  sqlite3_finalize(res);
  return count;
}

int database_add_ticket(Database *db, Ticket ticket) {
  char sql[SQL_MAXLEN];

  normalize_case(ticket.event);
  normalize_case(ticket.class);

  sqlite3_snprintf(sizeof sql, sql,
                   "INSERT OR REPLACE INTO TICKET (EVENT,CLASS,PRICE) "
                   "VALUES ('%s', '%s', %u);",
                   ticket.event, ticket.class, ticket.price);

  TRY(database_query(db, sql));
  return EXIT_SUCCESS;
}

int database_delete_ticket(Database *db, int64_t id) {
  char sql[SQL_MAXLEN];
  sqlite3_snprintf(sizeof sql, sql, "DELETE FROM TICKET WHERE TICKET_ID = %d",
                   id);

  TRY(database_query(db, sql));

  return EXIT_SUCCESS;
}

// Refreshes the data in the database
// -> Refreshes the cached tickets
static void database_refresh(Database *db) {
  assert(db);

  {
    // Get available tickets
    size_t ticket_count = database_get_rowcount(db, "TICKET");

    // Allocate space
    if (ticket_count >= db->ticket_count) {
      db->tickets = realloc(db->tickets, sizeof(*db->tickets) * ticket_count);
    }
    db->ticket_count = ticket_count;

    sqlite3_stmt *res = database_query_result(
        db, "SELECT TICKET_ID, EVENT, CLASS, PRICE FROM TICKET;");

    for (size_t i = 0; i < ticket_count; i++) {
      if (sqlite3_step(res) != SQLITE_ROW)
        break;

      db->tickets[i].id = sqlite3_column_int64(res, 0);
      snprintf(db->tickets[i].event, NAME_LEN, "%s",
               sqlite3_column_text(res, 1));
      snprintf(db->tickets[i].class, NAME_LEN, "%s",
               sqlite3_column_text(res, 2));
      db->tickets[i].price = sqlite3_column_int64(res, 3);
    }

    sqlite3_finalize(res);
  }

  {
    // Get sales info
    size_t ticket_sales_count =
        database_get_int(db, "select count(*) from TICKET;");

    // Allocate space
    if (ticket_sales_count >= db->ticket_sale_count) {
      db->ticket_sales = realloc(db->ticket_sales, sizeof(*db->ticket_sales) *
                                                       ticket_sales_count);
    }
    db->ticket_sale_count = ticket_sales_count;

    sqlite3_stmt *res = database_query_result(
        db, "select ticket.*, count(purchase_tickets.ticket_id) "
            "from ticket left join purchase_tickets on ticket.ticket_id = "
            "purchase_tickets.ticket_id "
            "group by ticket.ticket_id;");

    for (size_t i = 0; i < ticket_sales_count; i++) {
      if (sqlite3_step(res) != SQLITE_ROW)
        break;

      db->ticket_sales[i].ticket.id = sqlite3_column_int64(res, 0);
      snprintf(db->ticket_sales[i].ticket.event, NAME_LEN, "%s",
               sqlite3_column_text(res, 1));
      snprintf(db->ticket_sales[i].ticket.class, NAME_LEN, "%s",
               sqlite3_column_text(res, 2));
      db->ticket_sales[i].ticket.price = sqlite3_column_int64(res, 3);
      db->ticket_sales[i].count = sqlite3_column_int64(res, 4);
    }

    sqlite3_finalize(res);
  }

  {
    // Get purchases
    size_t purchase_count = database_get_rowcount(db, "PURCHASE");

    // Allocate space
    if (purchase_count >= db->purchase_count) {
      db->purchases =
          realloc(db->purchases, sizeof(*db->purchases) * purchase_count);
    }
    db->purchase_count = purchase_count;

    sqlite3_stmt *res = database_query_result(
        db, "SELECT PURCHASE_ID, CUSTOMER, TIME, PRICE FROM PURCHASE;");

    for (size_t i = 0; i < purchase_count; i++) {
      if (sqlite3_step(res) != SQLITE_ROW)
        break;

      db->purchases[i].purchase_id = sqlite3_column_int64(res, 0);
      snprintf(db->purchases[i].name, NAME_LEN, "%s",
               sqlite3_column_text(res, 1));
      snprintf(db->purchases[i].time, NAME_LEN, "%s",
               sqlite3_column_text(res, 2));
      db->purchases[i].price = sqlite3_column_int64(res, 3);
    }

    sqlite3_finalize(res);
  }

  // Get refunds
  {
    size_t refund_count = database_get_rowcount(db, "REFUND");

    // Allocate space
    if (refund_count >= db->refund_count) {
      db->refunds = realloc(db->refunds, sizeof(*db->refunds) * refund_count);
    }
    db->refund_count = refund_count;

    sqlite3_stmt *res =
        database_query_result(db, "SELECT REFUND_ID, CUSTOMER, PRICE, "
                                  "PURCHASE_TIME, REFUND_TIME FROM REFUND;");

    for (size_t i = 0; i < refund_count; i++) {
      if (sqlite3_step(res) != SQLITE_ROW)
        break;

      db->refunds[i].refund_id = sqlite3_column_int64(res, 0);
      snprintf(db->refunds[i].name, NAME_LEN, "%s",
               sqlite3_column_text(res, 1));
      db->refunds[i].price = sqlite3_column_int64(res, 2);
      snprintf(db->refunds[i].purchase_time, NAME_LEN, "%s",
               sqlite3_column_text(res, 3));
      snprintf(db->refunds[i].refund_time, NAME_LEN, "%s",
               sqlite3_column_text(res, 4));
    }

    sqlite3_finalize(res);
  }

  // Not out of date any more
  db->out_of_date = false;
}

size_t database_get_ticket_count(Database *db) {
  if (db->out_of_date)
    database_refresh(db);
  return db->ticket_count;
}

// Returns an array of all tickets
// Returned array should not be freed
// Results are cached and updated when changed
Ticket *database_get_tickets(Database *db) {
  if (db->out_of_date)
    database_refresh(db);
  return db->tickets;
}

int64_t database_add_purchase(Database *db, const char *name, Ticket *tickets,
                              size_t ticket_count) {
  int64_t total_price = 0;

  // Calculate and store total price for easy retrieval
  for (Ticket *p = tickets, *end = tickets + ticket_count; p != end; p++) {
    total_price += p->price;
  }

  char sql[SQL_MAXLEN];
  sqlite3_snprintf(sizeof sql, sql,
                   "INSERT INTO PURCHASE (CUSTOMER, PRICE) VALUES('%s', %d)",
                   name, total_price);

  database_query(db, sql);

  // Get the inserted id
  size_t purchase_id = database_get_int(db, "SELECT last_insert_rowid()");

  log_info("Referencing purchase %lld", purchase_id);

  // Construct the relational table linking purchase to tickets
  for (Ticket *ticket = tickets, *end = tickets + ticket_count; ticket != end;
       ticket++) {
    sqlite3_snprintf(sizeof sql, sql,
                     "INSERT INTO PURCHASE_TICKETS(PURCHASE_ID, TICKET_ID) "
                     "VALUES(%lld, %lld)",
                     purchase_id, ticket->id);

    database_query(db, sql);
  }

  return purchase_id;
}

int database_refund(Database *db, int64_t purchase_id) {
  char sql[SQL_MAXLEN];

  // Add the purchase to the refunds table copying most data and keeping track
  // of purchase and refund time
  sqlite3_snprintf(sizeof sql, sql,
                   "INSERT INTO REFUND (CUSTOMER, PRICE, PURCHASE_TIME) SELECT "
                   "purchase.customer, purchase.price, purchase.time from "
                   "purchase where purchase.purchase_id = %lld",
                   purchase_id);

  TRY(database_query(db, sql));

  // Remove the purchase from the purchases table
  sqlite3_snprintf(sizeof sql, sql,
                   "DELETE FROM PURCHASE WHERE purchase_id = %lld",
                   purchase_id);

  TRY(database_query(db, sql));

  // Delete the references of the purchase tickets relational table
  sqlite3_snprintf(sizeof sql, sql,
                   "DELETE FROM PURCHASE_TICKETS WHERE purchase_id = %lld",
                   purchase_id);

  TRY(database_query(db, sql));

  return EXIT_SUCCESS;
}

TicketSale *database_get_ticket_sales(Database *db) {
  if (db->out_of_date)
    database_refresh(db);

  return db->ticket_sales;
}

size_t database_get_ticket_sale_count(Database *db) {
  if (db->out_of_date)
    database_refresh(db);

  return db->ticket_sale_count;
}

PurchaseInfo *database_get_purchases(Database *db) {
  if (db->out_of_date)
    database_refresh(db);

  return db->purchases;
}

size_t database_get_purchase_count(Database *db) {
  if (db->out_of_date)
    database_refresh(db);

  return db->purchase_count;
}

RefundInfo *database_get_refunds(Database *db) {
  if (db->out_of_date)
    database_refresh(db);

  return db->refunds;
}

size_t database_get_refund_count(Database *db) {
  if (db->out_of_date)
    database_refresh(db);

  return db->refund_count;
}

PurchaseInfo database_get_purchase_info(Database *db, int64_t purchase_id) {
  char sql[SQL_MAXLEN];
  sqlite3_snprintf(sizeof sql, sql,
                   "SELECT PURCHASE_ID, CUSTOMER, TIME, PRICE FROM PURCHASE "
                   "WHERE PURCHASE_ID = %lld;",
                   purchase_id);

  sqlite3_stmt *res = database_query_result(db, sql);

  PurchaseInfo purchase = (PurchaseInfo){0};
  if (sqlite3_step(res) != SQLITE_ROW)
    return purchase;

  purchase.purchase_id = sqlite3_column_int64(res, 0);
  snprintf(purchase.name, NAME_LEN, "%s", sqlite3_column_text(res, 1));
  snprintf(purchase.time, NAME_LEN, "%s", sqlite3_column_text(res, 2));
  purchase.price = sqlite3_column_int64(res, 3);

  sqlite3_finalize(res);
  return purchase;
}

void database_destroy(Database *db) {
  sqlite3_close(db->sqlite);
  if (db->tickets)
    free(db->tickets);

  if (db->ticket_sales)
    free(db->ticket_sales);

  if (db->purchases)
    free(db->purchases);

  if (db->refunds)
    free(db->refunds);

  free(db);
}
