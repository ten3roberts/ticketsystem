#include "database.h"
#include <ctype.h>
#include <string.h>

// Sort utility function for comparing tickets based on event
int sort_event(const void *a, const void *b) {
  return strcmp(((const Ticket *)a)->event, ((const Ticket *)b)->event);
}

// Sort utility function for comparing tickets based on class
int sort_class(const void *a, const void *b) {
  return strcmp(((const Ticket *)a)->class, ((const Ticket *)b)->class);
}

// Sort utility function for comparing tickets based on price
int sort_price(const void *a, const void *b) {
  return (int)(((const Ticket *)a)->price - ((const Ticket *)b)->price);
}

// Converts the passed string into inital uppercase and rest lowercase
// characters
void normalize_case(char *str) {
  *str = (char)toupper(*str);
  for (char *p = str + 1; *p != '\0'; p++) {
    *p = (char)tolower(*p);
  }
}
