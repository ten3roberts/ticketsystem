#include "log.h"
#include <stdbool.h>
#include "notification.h"
#include <stdio.h>
#include <time.h>

#ifdef _WIN32
// Don't include everything
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#endif

static bool log_has_init = false;

const char *log_level_strmap[] = {
    [LOG_INFO] = "INFO", [LOG_WARN] = "WARNING", [LOG_ERROR] = "ERROR"};

const char *log_level_stylemap[LOG_LEVEL_COUNT] = {
    [LOG_INFO] = "\033[1;32m",  // Bold green
    [LOG_WARN] = "\033[1;33m",  // Bold yellow
    [LOG_ERROR] = "\033[1;31m", // Bold red
};

#ifdef _WIN32
static void log_configure_win32console(HANDLE hconsole) {
  DWORD dw_mode = 0;
  if (!GetConsoleMode(hconsole, &dw_mode)) {
    fprintf(stderr, "Failed to get console mode\n");
    return;
  }

  dw_mode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING;

  if (!SetConsoleMode(hconsole, dw_mode)) {
    fprintf(stderr, "Failed to set console mode\n");
    return;
  }
}
#endif

// Automatically initializes logging
static void log_init() {
  // Don't initialize twice
  if (log_has_init)
    return;

  log_has_init = true;
  // Tell windows to recognised ansi escape sequences
#ifdef _WIN32
  HANDLE hstdout = GetStdHandle(STD_OUTPUT_HANDLE);
  HANDLE hstderr = GetStdHandle(STD_ERROR_HANDLE);

  if (hstdout == INVALID_HANDLE_VALUE) {
    fprintf(stderr, "Failed to get output handle to console\n");
    return;
  }

  if (hstderr == INVALID_HANDLE_VALUE) {
    fprintf(stderr, "Failed to get error handle to console\n");
    return;
  }

  log_configure_win32console(hstdout);
  log_configure_win32console(hstderr);

#endif
}

void log_call(LogLevel level, const char *fmt, ...) {
  va_list args;
  va_start(args, fmt);
  log_callva(level, fmt, args);
  va_end(args);
}

void log_callva(LogLevel level, const char *fmt, va_list args) {
  log_init();

  time_t now = time(NULL);
  struct tm *timeinfo = localtime(&now);
  char timestamp[256];
  strftime(timestamp, sizeof timestamp, "%F %H.%M.%S", timeinfo);

  const char *levelstr = log_level_strmap[level];
  const char *stylestr = log_level_stylemap[level];

  FILE *stream = stdout;

  // Print to stderr if warning or error
  if (level >= LOG_WARN) {
    stream = stderr;
  }

  // Print header
  fprintf(stream, "[%s%s\033[0;m %s]: ", stylestr, levelstr, timestamp);

  // Message
  vfprintf(stream, fmt, args);

  // Newline
  fputc('\n', stream);
}
