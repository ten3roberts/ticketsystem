#include "notification.h"
#include "style.h"
#include <stdio.h>
#include <time.h>
#include "log.h"

#define MSG_LEN 256
#define NOTIFICATIONS_MAX 8
#define NOTIFICATION_DURATION 5

struct Notification {
  time_t create_time;
  enum NotificationType type;

  char msg[MSG_LEN];
};

// Behaves like a fixed size ring buffer
// This allows notifications to be created and destroyed with O(1)
static struct Notification notifications[NOTIFICATIONS_MAX];
// The offset into the ring for the 'first' notification
static size_t notification_index = 0;
// The number of notifications
static size_t notification_count = 0;

void notification_send(enum NotificationType type, const char *msg) {
  // Get the position in the ring to insert the notification
  size_t index = (notification_index + notification_count) % NOTIFICATIONS_MAX;
  struct Notification *cur = notifications + index;

  snprintf(cur->msg, sizeof cur->msg, "%s", msg);
  cur->type = type;
  cur->create_time = time(NULL);

  notification_count++;
}

// Removes the first notification, the oldest
static void notification_dequeue() {
  notification_index = (notification_index + 1) % NOTIFICATIONS_MAX;
  notification_count--;
}

void notification_draw(struct nk_context *ctx) {
  if (notification_count == 0) {
    return;
  }

  // Remove first notification if it is old enough
  if (time(NULL) >
      notifications[notification_index].create_time + NOTIFICATION_DURATION) {
    notification_dequeue();
  }

  /* struct nk_style_item c = ctx->style.tab */

  nk_layout_row_dynamic(ctx, HEADER_HEIGHT, 1);
  nk_button_label(ctx, "Notifications");

  nk_layout_row_dynamic(ctx, NOTIFICATION_HEIGHT, 1);

  for (size_t i = 0; i < notification_count; i++) {
    struct nk_color color = nk_rgba(255, 255, 255, 255);
    struct Notification *cur = notifications + (i + notification_index) % NOTIFICATIONS_MAX;
    if (cur->type == NOTIFY_OK) {
      color = nk_rgba(10, 214, 126, 255);
    } else if (cur->type == NOTIFY_ERROR) {
      color = nk_rgba(214, 10, 115, 255);
    }

    nk_label_colored(ctx, cur->msg, NK_TEXT_LEFT, color);
  }

  // Some spacing
  nk_label(ctx, "", NK_TEXT_LEFT);
}
