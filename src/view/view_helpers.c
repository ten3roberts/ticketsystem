#include "nuklear_include.h"
#include "style.h"

void ui_header(struct nk_context *ctx, const char *title) {
  nk_layout_row_dynamic(ctx, HEADER_HEIGHT, 1);
  nk_label(ctx, title, NK_TEXT_LEFT);
}

void ui_widget(struct nk_context *ctx, float height) {
  static const float ratio[] = {0.15f, 0.85f};
  nk_layout_row(ctx, NK_DYNAMIC, height, 2, ratio);
  nk_spacing(ctx, 1);
}

void ui_widget_centered(struct nk_context *ctx, float height) {
  static const float ratio[] = {0.15f, 0.50f, 0.35f};
  nk_layout_row(ctx, NK_DYNAMIC, height, 3, ratio);
  nk_spacing(ctx, 1);
}

int ui_tab(struct nk_context *ctx, const char *title, int active) {
  struct nk_style_item c = ctx->style.button.normal;
  if (active) {
    ctx->style.button.normal = ctx->style.button.active;
  }
  int r = nk_button_label(ctx, title);
  ctx->style.button.normal = c;
  return r;
}
