#ifndef NOTIFICATION_H
#define NOTIFICATION_H

enum NotificationType {
 NOTIFY_OK,
 NOTIFY_ERROR,
};

#include "nuklear_include.h"
// Displays a notification for the default duration
void notification_send(enum NotificationType type, const char* msg);

// Draws notifications
// Also removes old notifications
void notification_draw(struct nk_context* ctx);

#endif /* NOTIFICATION_H */
