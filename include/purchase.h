#ifndef PURCHASE_H
#define PURCHASE_H

#include "controller.h"
#include "nuklear_include.h"
#include "database.h"

typedef struct ControllerPurchase {
  ControllerHeader header;
  char name[NAME_LEN];
  int current_sort;

  // Tickets for the current purchase
  size_t ticket_cap;
  size_t ticket_len;
  Ticket* tickets;
  int64_t price_total;
} ControllerPurchase;

ControllerPurchase controller_purchase_create();
void controller_purchase_destroy(ControllerPurchase* controller);

void controller_purchase_update(void *p, struct nk_context* nk_ctx, Database *db);
#endif
