#ifndef RENDERER_H
#define RENDERER_H

#include <stdbool.h>

typedef struct Renderer Renderer;

// Creates a renderer with a window of width and height and title
// Handles glfw and glew initialization
Renderer *renderer_create(int width, int height, const char *title);

// Starts next frame
// Returns false if window should close
bool renderer_begin(Renderer *renderer);
void renderer_end(Renderer *renderer);

// Destroys a renderer
// Handles glfw and glew termination
void renderer_destroy(Renderer *renderer);

struct nk_context *renderer_nk_ctx(Renderer *renderer);

#endif /* RENDERER_H */
