#include "controller.h"
#include "database.h"
#include "nuklear_include.h"

// This view and controller defines the interface for adding and viewing the ticket types
// Interacts with database

typedef struct ControllerTickets {
  ControllerHeader header;
  char input_event[NAME_LEN];
  char input_class[NAME_LEN];
  char input_price[NAME_LEN];
  int current_sort;
} ControllerTickets;

// Creates a new instance of the Ticket controller
ControllerTickets controller_tickets_create();

void controller_tickets_update(void* p, struct nk_context* nk_ctx, Database *db);
