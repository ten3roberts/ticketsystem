#ifndef CONTROLLER_H
#define CONTROLLER_H
#include "nuklear_include.h"
#include "database.h"

#define NAME_LEN 80

typedef struct Application Application;

// Receives itself as first argument
typedef void (*ControllerUpdateFunc)(void *, struct nk_context*, Database* db);

// Header for each controller
// Contains update function pointer
typedef struct ControllerHeader {
  ControllerUpdateFunc update_func;
} ControllerHeader;

#endif /* CONTROLLER_H */
