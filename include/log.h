#ifndef LOG_H
#define LOG_H

#include <stdarg.h>

typedef enum { LOG_INFO, LOG_WARN, LOG_ERROR, LOG_LEVEL_COUNT } LogLevel;

// Map the level int to a string literal
extern const char *log_level_strmap[];

extern const char *log_level_stylemap[LOG_LEVEL_COUNT];

#define log_info(...) log_call(LOG_INFO, __VA_ARGS__)
#define log_warn(...) log_call(LOG_WARN, __VA_ARGS__)
#define log_err(...) log_call(LOG_ERROR, __VA_ARGS__)

// Logs a formatted log message along with header to stdout or stderr
void log_call(LogLevel level, const char *fmt, ...);
// Vargs version
void log_callva(LogLevel level, const char *fmt, va_list args);
#endif
