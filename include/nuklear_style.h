#include "nuklear_include.h"
enum theme {THEME_BLACK, THEME_WHITE, THEME_RED, THEME_BLUE, THEME_DARK};

void
nuklear_set_style(struct nk_context* ctx, enum theme theme);