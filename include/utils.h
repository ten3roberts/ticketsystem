// Sorts ticket based on event
int sort_event(const void *a, const void *b);
// Sorts ticket based on class
int sort_class(const void *a, const void *b);
// Sorts ticket based on price
int sort_price(const void *a, const void *b);

// Converts the passed string into inital uppercase and rest lowercase
// characters
void normalize_case(char *str);

