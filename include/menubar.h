#include "nuklear_include.h"

// Draws a menubar with buttons for each item
// `items` is a list of strings and NULL for the last item
// Returns the item clicked or -1
int menubar_draw(struct nk_context *ctx, const char **items, int selected);
