#ifndef VIEW_HELPERS_H
#define VIEW_HELPERS_H
#include "nuklear_include.h"

void ui_header(struct nk_context *ctx, const char *title);

void ui_widget(struct nk_context *ctx, float height);

void ui_widget_centered(struct nk_context *ctx, float height);

// Shows a tab button
int ui_tab(struct nk_context* ctx, const char* title, int active);

#endif /* VIEW_HELPERS_H */
