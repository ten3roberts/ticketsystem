#ifndef REFUND_H
#define REFUND_H

#include "controller.h"
#include "nuklear_include.h"
#include "database.h"

typedef struct ControllerRefund {
  ControllerHeader header;
  char current_search[NAME_LEN];
} ControllerRefund;

ControllerRefund controller_refund_create();
void controller_refund_destroy(ControllerRefund* controller);

void controller_refund_update(void *p, struct nk_context* nk_ctx, Database *db);
#endif
