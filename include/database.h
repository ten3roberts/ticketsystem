// Defines the 'model' for the ticketsystem
// Interacts with the sqlite3 database and returns queries in c structs
#ifndef DATABASE_H
#define DATABASE_H
#include <stddef.h>
#include <stdint.h>

typedef struct Database Database;

#define NAME_LEN 80

// Defines a ticket type
typedef struct {
  int64_t id;
  // Event name for ticket
  char event[NAME_LEN];
  // Who this ticket type is meant for
  char class[NAME_LEN];
  // Price of ticket
  int64_t price;
} Ticket;

typedef struct {
  Ticket ticket;
  int64_t count;
} TicketSale;

// Holds information regarding a purchase
// Does not contain all tickets in purchase
typedef struct {
  int64_t purchase_id;
  char name[NAME_LEN];
  char time[NAME_LEN];
  int64_t price;
} PurchaseInfo;

// Holds information regarding a refunded purchase
// Does not contain all tickets in purchase
typedef struct {
  int64_t refund_id;
  char name[NAME_LEN];
  int64_t price;
  char purchase_time[NAME_LEN];
  char refund_time[NAME_LEN];
} RefundInfo;

Database *database_create(const char *filename);

// Creates the following tables:
// -> PURCHASE
// -> REFUND
// -> PURCHASE_TICKETS
// -> TICKET
// Returns EXIT_SUCCESS or EXIT_FAILURE
int database_create_tables(Database *db);

// Executes SQL query
// Returns EXIT_SUCCESS on success
int database_query(Database *db, const char *sql);

// Registers or updates a ticket type
// If a ticket with the same name and class exists, the existing ticket is
// updated and given a new id
int database_add_ticket(Database *db, Ticket ticket);

// Deletes the ticket matching event and class
int database_delete_ticket(Database *db, int64_t id);

// Returns the number of ticket types
size_t database_get_ticket_count(Database *db);

// Returns an array of all tickets
// Returned array should not be freed
// Results are cached and updated when changed
Ticket *database_get_tickets(Database *db);

// Adds a purchase to the database
// Returns the purchase_id
int64_t database_add_purchase(Database *db, const char *name, Ticket *tickets,
                              size_t ticket_count);

// Refunds a purchase
int database_refund(Database *db, int64_t purchase_id);

TicketSale *database_get_ticket_sales(Database *db);
size_t database_get_ticket_sale_count(Database *db);

// Returns a list containing information about a purchase
PurchaseInfo *database_get_purchases(Database *db);
size_t database_get_purchase_count(Database *db);

// Returns a list containing information about a refund
RefundInfo *database_get_refunds(Database *db);
size_t database_get_refund_count(Database *db);

PurchaseInfo database_get_purchase_info(Database *db, int64_t purchase_id);

// Free resources and database
void database_destroy(Database *db);

#endif /* DATABASE_H */
