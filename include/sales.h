#ifndef SALES_H
#define SALES_H
#include "database.h"
#include "controller.h"
#include "nuklear_include.h"

typedef struct ControllerSales {
  ControllerHeader header;
  int current_sort;
} ControllerSales;

ControllerSales controller_sales_create();

void controller_sales_update(void *p, struct nk_context* nk_ctx, Database* db);
#endif
