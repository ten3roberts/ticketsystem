# Backend
* Tickets with different price classes
* Notion of transaction
* Register transaction
* Lookup transaction by ID
* Lookup transaction by name
* Query number of sold tickets
* Query number of tickets by price class
* Save transactions to file
* Transactions are stored in a database

# Frontend
* GUI for registering transactions with configurable number of items
* GUI for reverting a transaction
* Search system by name, id, price class, or show
* Statistics window with total tickets sold for each priceclass and total sum

## Transaction
* List of items
* ID
* name (of family)

## Item
* Price class
* Event name
