# Ticketsystem
A C system for registering transactions for event tickets.

## Building
## Submodules
Before building make sure all submodules are checked out
```
git submodule init
git submodule update
```

```
mkdir build
cd build
cmake ..
make
```
